﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
        <title>TPE DISQUE DUR</title>
		
   </head>

    <body>

		<header>
			
			<p><img src="img/BF.png" alt="Banniere"/></p>

			<span class="ecriture"><a href="lecture,ecriture.html"><img src="img/ecriture_V.png" alt="logo écriture"/></a></span>

			<span class="transfert"><a href="DISQUE.html"><img src="img/plateaux.png" alt="logo Plateaux"/></a></span>

			<span class="mecanisme"><a href="mecanisme.html"><img src="img/Mecanisme_R.png" alt="logo Mécanisme"/></a></span>

			<span class="histoire"><a href="index.html"><img src="img/HistoireL.png" alt="logo Histoire"/></a></span>


			<span class="tetep"><a href="tete_de_lecture.html"><img src="img/Tetes_de_Lecture.png" alt="logo tête de lecture"/></a></span>

			<span class="webo"><a href="webographie.html"><img src="img/WebographieL.png" alt="logo webographie"/></a></span>
			
		
		</header>		

			
			
			<?php
			
			echo '<span class="code">';

			if(!isset($_POST['mdp']) || $_POST['mdp'] != "GayLussac"){
				?>
				<form action="accueilP.php" method="post">
					<p>
					<label>Mot de passe: </label><input type="password" name="mdp" />
					<input type="submit" value="Envoyer" />
					</p>
				</form>
			</span>	

				<?php
			}
			else{
			?>
				
				
				<section>
					
					<iframe width="560" height="315" src="http://www.youtube.com/embed/LV4f7bODIH4" frameborder="0" allowfullscreen></iframe>
					
					<br/>Bonjour,<br/>
					Nous proposons notre TPE traîtant le disque dur sur ce site entièrement réalisé à la main ( images, schémas, vidéos inclus ).<br/>
					Si toutefois vous voudriez regarder de plus près nos créations, nous vous proposons <a href="site tpe.zip">de télécharger cette archive</a>
					contenant tout notre site ainsi qu'un utilitaire afin de mieux vous repérer dans les lignes de codes.<br/>
					<br/><strong>Problématique :</strong> Comment se réalise le stockage des données sur un disque dur ?<br/><br/>
					<strong>PLAN</strong><br/><br/>
					I) Historique<br/>
					II) Structure du disque dur<br/>
					III) Têtes de lectures, écriture et lecture des données<br/>
					IV) Webographie<br/>

			<?php
			}
			?>	
			
			<a href="index.html"><img id="suivant" src="img/electronique.png" alt="Suivant"></a>
		</section>
		
		<footer>
			<img src="img/pied_de_page.jpg" alt="Pied de page"/>
		</footer>

	</body>
</html>